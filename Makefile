dir = $(shell pwd)

default:
	-docker network create digilab-demo-frk

build:
	docker-compose build

up: default sqlc
	docker-compose up -d --remove-orphans

logs:
	docker-compose logs -f

down:
	docker-compose down --remove-orphans

sqlc:
	docker run --rm -v $(dir)/pkg/storage:/src -w /src/queries kjconroy/sqlc:1.20.0 generate

sqlc-vet:
	docker run --rm -v $(dir)/pkg/storage:/src -w /src/queries kjconroy/sqlc:1.20.0 vet 
