package adapter

import (
	"database/sql"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-kadaster/pkg/model"
	queries "gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-kadaster/pkg/storage/queries/generated"
)

func ToPlot(record *queries.DigilabDemoFrkPlot) (*model.Plot, error) {
	return &model.Plot{
		ID: record.ID,
		Municipality: model.Municipality{
			Name: record.MunicipalityName.String,
			Code: record.MunicipalityCode.String,
		},
		Section: record.Section.String,
		Number:  record.Number.Int32,
		Surface: record.Surface.Int32,
	}, nil
}

func ToPlots(records []*queries.DigilabDemoFrkPlot) ([]model.Plot, error) {
	plots := make([]model.Plot, 0, len(records))
	for idx := range records {
		plot, err := ToPlot(records[idx])
		if err != nil {
			return nil, err
		}
		plots = append(plots, *plot)
	}

	return plots, nil
}

func ToPlotPurchase(personID, plotID uuid.UUID) *queries.PlotPurchaseParams {
	return &queries.PlotPurchaseParams{
		PersonID: personID,
		PlotID:   plotID,
	}
}

func ToPlotSell(personID, plotID uuid.UUID) *queries.PlotSellParams {
	return &queries.PlotSellParams{
		PersonID: personID,
		PlotID:   plotID,
	}
}

func FromPlotCreate(plot *model.NewPlotArgs) *queries.PlotCreateParams {
	return &queries.PlotCreateParams{
		ID:               plot.ID,
		MunicipalityCode: sql.NullString{String: plot.Municipality.Code, Valid: true},
		MunicipalityName: sql.NullString{String: plot.Municipality.Name, Valid: true},
		Section:          sql.NullString{String: plot.Section, Valid: true},
		Number:           sql.NullInt32{Int32: plot.Number, Valid: true},
		Surface:          sql.NullInt32{Int32: plot.Surface, Valid: true},
	}
}

func FromPlotUpdate(id uuid.UUID, plot *model.Plot) *queries.PlotUpdateParams {
	return &queries.PlotUpdateParams{
		ID:               id,
		MunicipalityCode: sql.NullString{String: plot.Municipality.Code, Valid: true},
		MunicipalityName: sql.NullString{String: plot.Municipality.Name, Valid: true},
		Section:          sql.NullString{String: plot.Section, Valid: true},
		Number:           sql.NullInt32{Int32: plot.Number, Valid: true},
		Surface:          sql.NullInt32{Int32: plot.Surface, Valid: true},
	}
}

func FromPlotPatch(id uuid.UUID, patch *model.PlotPatch, plot *model.Plot) *queries.PlotUpdateParams {
	args := &queries.PlotUpdateParams{
		ID: id,
	}

	municipalityCode := plot.Municipality.Code
	municipalityName := plot.Municipality.Name
	section := plot.Section
	number := plot.Number
	surface := plot.Surface

	if patch.Municipality != nil {
		municipalityCode = patch.Municipality.Code
		municipalityName = patch.Municipality.Name
	}

	if patch.Section != nil {
		section = *patch.Section
	}

	if patch.Number != nil {
		number = *patch.Number
	}

	if patch.Surface != nil {
		surface = *patch.Surface
	}

	args.MunicipalityCode = sql.NullString{String: municipalityCode, Valid: true}
	args.MunicipalityName = sql.NullString{String: municipalityName, Valid: true}
	args.Section = sql.NullString{String: section, Valid: true}
	args.Number = sql.NullInt32{Int32: number, Valid: true}
	args.Surface = sql.NullInt32{Int32: surface, Valid: true}

	return args
}

func FromPlotPatchToCreate(id uuid.UUID, patch *model.PlotPatch) *model.NewPlotArgs {
	plot := &model.NewPlotArgs{
		ID: id,
	}

	if patch.Municipality != nil {
		plot.Municipality = *patch.Municipality
	}

	if patch.Section != nil {
		plot.Section = *patch.Section
	}

	if patch.Number != nil {
		plot.Number = *patch.Number
	}

	if patch.Surface != nil {
		plot.Surface = *patch.Surface
	}

	return plot
}

func ToBuildOnPlot(item *model.BuildOnPlot) *queries.BuildOnPlotParams {
	return &queries.BuildOnPlotParams{
		BuildingID: item.BuildingID,
		PlotID:     item.PlotID,
	}
}
