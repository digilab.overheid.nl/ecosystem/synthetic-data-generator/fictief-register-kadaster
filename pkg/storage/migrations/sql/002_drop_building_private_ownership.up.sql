BEGIN TRANSACTION;

DROP TABLE digilab_demo_frk.private_owner_building;

ALTER TABLE digilab_demo_frk.plots DROP COLUMN number;

ALTER TABLE digilab_demo_frk.plots ADD COLUMN number INT;
ALTER TABLE digilab_demo_frk.plots ADD COLUMN surface INT;
 
COMMIT;