BEGIN transaction;

CREATE SCHEMA digilab_demo_frk;

CREATE TABLE digilab_demo_frk.plots (
    id UUID NOT NULL,
    municipality TEXT,
    section TEXT,
    number TEXT,

    PRIMARY KEY(id)
);

CREATE TABLE digilab_demo_frk.buildings (
    id UUID NOT NULL,
    constructed_at TIMESTAMP,
    purpose TEXT,
    surface INT,

    PRIMARY KEY(id)
);

CREATE TABLE digilab_demo_frk.people (
    id UUID NOT NULL,
    bsn TEXT UNIQUE,

    PRIMARY KEY(id)   
);

CREATE TABLE digilab_demo_frk.private_owner_plot (
    person_id UUID NOT NULL,
    plot_id UUID NOT NULL,

    CONSTRAINT fk_private_owner_plot_person_id FOREIGN KEY(person_id) REFERENCES digilab_demo_frk.people(id),
    CONSTRAINT fk_private_owner_plot_id FOREIGN KEY(plot_id) REFERENCES digilab_demo_frk.plots(id)
);

CREATE TABLE digilab_demo_frk.private_owner_building (
    person_id UUID NOT NULL, 
    building_id UUID NOT NULL,

    CONSTRAINT fk_private_owner_building_person_id FOREIGN KEY(person_id) REFERENCES digilab_demo_frk.people(id),
    CONSTRAINT fk_private_owner_building_id FOREIGN KEY(building_id) REFERENCES digilab_demo_frk.buildings(id)
);

CREATE TABLE digilab_demo_frk.building_plot (
    building_id UUID NOT NULL,
    plot_id UUID NOT NULL,

    PRIMARY KEY(building_id, plot_id),

    CONSTRAINT fk_building_plot_building_id_1 FOREIGN KEY(building_id) REFERENCES digilab_demo_frk.buildings(id),
    CONSTRAINT fk_building_plot_plot_id_1 FOREIGN KEY(plot_id) REFERENCES digilab_demo_frk.plots(id)
);

COMMIT;
