-- name: PlotPurchase :exec
INSERT INTO digilab_demo_frk.private_owner_plot (
    person_id, plot_id
)
VALUES ($1, $2);

-- name: PlotSell :exec
DELETE FROM digilab_demo_frk.private_owner_plot
WHERE person_id=$1 AND plot_id=$2;

-- name: PeoplePlot :many
SELECT person_id
FROM digilab_demo_frk.private_owner_plot 
WHERE plot_id=$1;