-- name: PlotList :many
SELECT id, municipality_name, section, number, surface, municipality_code
FROM digilab_demo_frk.plots;

-- name: PlotGet :one
SELECT id, municipality_name, section, number, surface, municipality_code
FROM digilab_demo_frk.plots
WHERE id=$1;

-- name: PlotCreate :exec
INSERT INTO digilab_demo_frk.plots (
    id, municipality_name, section, number, surface, municipality_code
)
VALUES ($1, $2, $3, $4, $5, $6);

-- name: PlotUpdate :exec
UPDATE digilab_demo_frk.plots
SET municipality_name=$1, municipality_code=$2, section=$3, number=$4, surface=$5
WHERE id=$6;

-- name: BuildOnPlot :exec
INSERT INTO digilab_demo_frk.building_plot (
    building_id, plot_id
)
VALUES ($1, $2);

-- name: PlotsBuilding :many
SELECT id, municipality_name, section, number, surface, municipality_code
FROM digilab_demo_frk.plots
LEFT JOIN digilab_demo_frk.building_plot as bp
    ON plots.id=bp.plot_id 
WHERE bp.building_id=$1;

-- name: PlotsPrivateOwner :many
SELECT id, municipality_name, section, number, surface, municipality_code
FROM digilab_demo_frk.plots
LEFT JOIN digilab_demo_frk.private_owner_plot as owner
    ON plots.id=owner.plot_id
WHERE owner.person_id=$1;