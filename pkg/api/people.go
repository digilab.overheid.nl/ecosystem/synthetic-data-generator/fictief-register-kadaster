package api

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-kadaster/pkg/storage/adapter"
)

func (a *API) PeopleGet(w http.ResponseWriter, r *http.Request) {
	id, err := uuid.Parse(chi.URLParam(r, "personID"))
	if err != nil {
		writeError(w, err)
		return
	}

	records, err := a.db.Queries.PlotsPrivateOwner(r.Context(), id)
	if err != nil {
		writeError(w, err)
		return
	}

	plots, err := adapter.ToPlots(records)
	if err != nil {
		writeError(w, err)
		return
	}

	if err := json.NewEncoder(w).Encode(plots); err != nil {
		writeError(w, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
