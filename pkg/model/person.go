package model

import "github.com/google/uuid"

type Owners struct {
	Owners []uuid.UUID `json:"owners"`
}
