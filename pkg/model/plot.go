package model

import "github.com/google/uuid"

type Municipality struct {
	Name string `json:"name"`
	Code string `json:"code"`
}

type Plot struct {
	ID           uuid.UUID    `json:"id"`
	Municipality Municipality `json:"municipality"`
	Section      string       `json:"section"`
	Number       int32        `json:"number"`
	Surface      int32        `json:"surface"`
	Buildings    []Building   `json:"buildings,omitempty"`
	Owners       []uuid.UUID  `json:"owners,omitempty"`
}

type NewPlotArgs struct {
	ID           uuid.UUID    `json:"id"`
	Municipality Municipality `json:"municipality"`
	Section      string       `json:"section"`
	Number       int32        `json:"number"`
	Surface      int32        `json:"surface"`
}

type PlotPatch struct {
	ID           uuid.UUID     `json:"id"`
	Municipality *Municipality `json:"municipality"`
	Section      *string       `json:"section"`
	Number       *int32        `json:"number"`
	Surface      *int32        `json:"surface"`
}

type Reparcelled struct {
	From []uuid.UUID `json:"from"`
	To   []PlotPatch `json:"to"`
}

type BuildOnPlot struct {
	BuildingID uuid.UUID `json:"buildingID"`
	PlotID     uuid.UUID `json:"plotID"`
}
